use std::error::Error;
use std::fs;
use std::io::{Read, Write};
use std::num::ParseIntError;
/// Git helper
///
/// @author Pierre Hubert

use std::path::Path;
use std::process::{Child, Command, Stdio};

use futures::io::ErrorKind;

use crate::structures::{CommitMetadata, DiffFile, DiffFileSection, DiffIndexHeader, ExtendedDiffHeader, FullCommit, TreeEntry};

/// Check out whether a given directory is a valid Git directory
/// or not
pub fn is_git_dir(path: &str) -> bool {
    let files_to_check = [
        format!("{}HEAD", path)
    ];

    let dirs_to_check = [
        format!("{}objects", path),
        format!("{}refs", path),
    ];

    !files_to_check.iter().any(|f| !Path::new(f).is_file()) &&
        !dirs_to_check.iter().any(|f| !Path::new(f).is_dir())
}

/// Create a new bare repository
pub fn create_bare_repo(path: &str) -> std::io::Result<()> {
    println!("Create a new Git bare repository at {}", path);

    if !Command::new("git")
        .arg("init")
        .arg("--bare")
        .arg(path)
        .output()?.status.success() {
        return Err(std::io::Error::new(ErrorKind::Other, "Could not create repository"));
    }

    Ok(())
}

/// Get the list of repositories in a given path
pub fn list_git_repos(path: &str) -> std::io::Result<Vec<String>> {
    let mut list: Vec<String> = Vec::new();

    for path in fs::read_dir(path)? {
        let path = path?.path();

        if !path.is_dir() {
            continue;
        }

        let path = path.to_str().expect("could not turn path into str");
        let trailing: Vec<&str> = path.split('/').collect();
        let trailing = trailing.last().unwrap();
        let path = format!("{}/", path); // Add end slash


        // Check if it is a git directory
        match is_git_dir(&path) {
            true => list.push(trailing.to_string()),

            // Else recurse search
            false => {
                for sub_path in list_git_repos(&path)? {
                    list.push(format!("{}/{}", trailing, sub_path));
                }
            }
        };
    }

    Ok(list)
}


fn start_cmd(path: &str) -> Command {
    let mut cmd = Command::new("git");
    cmd.arg(format!("--git-dir={}", path));
    cmd
}

fn start_upload(mut cmd: Command, req: Option<&[u8]>) -> std::io::Result<Child> {
    let mut child = cmd
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    // Check for data to put in stdin
    if let Some(req) = req {
        // Put stdin
        let stdin = child.stdin.as_mut().ok_or_else(||
            std::io::Error::new(ErrorKind::BrokenPipe, "failed to open stdin"))?;
        stdin.write_all(req)?;
    }


    Ok(child)
}

/// Check if a file exists in the git repository
pub fn file_exists(path: &str, revision: &str, file: &str) -> std::io::Result::<bool> {
    Ok(list_files(path, file, revision)?.get(0).is_some())
}

/// Get the list of the files in a given Git repository
pub fn list_files(git_path: &str, path: &str, revision: &str) -> std::io::Result<Vec<TreeEntry>> {
    let mut path = path.to_string();
    if path.starts_with('/') {
        path = format!(".{}", path)
    }

    let output = start_cmd(git_path)
        .arg("ls-tree")
        .arg("-l")
        .arg(revision)
        .arg(path)
        .output()?;

    let output = String::from_utf8_lossy(&output.stdout);

    let files: Vec<&str> = output.split('\n').collect::<Vec<&str>>();

    Ok(files
        .iter()
        .map(|f| f.trim())
        .filter(|f| !f.is_empty())
        .map(|f| {
            let mut line = f.replace('\t', " ");
            while line.contains("  ")
            {
                line = line.replace("  ", " ");
            }

            let line = line.splitn(5, ' ').filter(|f| !f.is_empty()).collect::<Vec<&str>>();

            TreeEntry {
                is_dir: !line[1].eq("blob"),
                size: line[3].parse::<u64>().unwrap_or(0),
                object: line[2].to_string(),
                file_name: line[4].to_string(),
            }
        })
        .collect::<Vec<TreeEntry>>())
}

/// Get the list of branches in a given repository
pub fn list_branches(path: &str) -> std::io::Result<Vec<String>> {
    let output = start_cmd(path)
        .arg("branch")
        .output()?;

    let output = String::from_utf8_lossy(&output.stdout).to_string();

    let output = output.split('\n')
        .map(|f| f.to_string())
        .map(|f| f.trim().replace("* ", ""))
        .filter(|f| !f.is_empty())
        .collect::<Vec<String>>();

    Ok(output)
}

/// Get the number of commits of a revision
pub fn count_commits(path: &str, rev: &str) -> std::io::Result<u64> {
    let output = start_cmd(path)
        .arg("rev-list")
        .arg("--count")
        .arg(rev)
        .output()?;

    let output = String::from_utf8_lossy(&output.stdout).to_string();

    Ok(output.lines().next().unwrap().parse::<u64>().unwrap_or(0))
}


/// Get the active branch of a repo
pub fn default_branch(path: &str) -> std::io::Result<String> {
    let output = start_cmd(path)
        .arg("branch")
        .output()?;

    let stdout = String::from_utf8_lossy(&output.stdout).to_string();

    let branch = stdout.split('\n')
        .map(|f| f.to_string())
        .find(|f| f.starts_with("* "))
        .map(|f| f.trim().replace("* ", ""));

    if let Some(b) = branch {
        return Ok(b);
    }

    let branches = stdout.split('\n')
        .map(|f| f.trim())
        .collect::<Vec<_>>();

    if branches.contains(&"master") {
        return Ok("master".to_string());
    }

    if branches.contains(&"main") {
        return Ok("main".to_string());
    }

    Ok(branches.first().unwrap_or(&"master").to_string())
}

/// Get the list of tags in a given repository
pub fn list_tags(path: &str) -> std::io::Result<Vec<String>> {
    let output = start_cmd(path)
        .arg("tag")
        .output()?;

    let output = String::from_utf8_lossy(&output.stdout).to_string();

    let output = output.split('\n')
        .map(|f| f.to_string())
        .map(|f| f.trim().replace("* ", ""))
        .filter(|f| !f.is_empty())
        .collect::<Vec<String>>();

    Ok(output)
}

/// Get the content of file from the repository
pub fn get_file_content(path: &str, revision: &str, file: &str) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("show")
        .arg(format!("{}:{}", revision, file));

    start_upload(cmd, None)
}

/// Get the repo as an archive
pub fn archive_repo(path: &str, revision: &str, format: &str) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("archive")
        .arg(format!("--format={}", format))
        .arg(revision);

    start_upload(cmd, None)
}

/// Get git references for upload
pub fn get_upload_refs(path: &str) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("upload-pack")
        .arg("--stateless-rpc")
        .arg("--advertise-refs")
        .arg(path);

    start_upload(cmd, None)
}

/// Get git references for download
pub fn get_receive_refs(path: &str) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("receive-pack")
        .arg("--stateless-rpc")
        .arg("--advertise-refs")
        .arg(path);

    start_upload(cmd, None)
}

/// Upload a pack
pub fn git_upload_pack(path: &str, req: &[u8]) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("upload-pack")
        .arg("--stateless-rpc")
        .arg("--timeout")
        .arg("2")
        .arg(path);

    start_upload(cmd, Some(req))
}

/// Start to receive a pack
///
/// Returns a child that must be used to write request & read response
pub fn git_receive_pack(path: &str) -> std::io::Result<Child> {
    let mut cmd = start_cmd(path);
    cmd.arg("receive-pack")
        .arg("--stateless-rpc")
        .arg(path)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped());
    cmd.spawn()
}

/// Parse a list of commits metadata returned by the execution of a Git command
fn parse_commits_metadata_output(output: &std::process::Output)
                                 -> Result<Vec<CommitMetadata>, Box<dyn Error>> {
    let str = String::from_utf8_lossy(&output.stdout).to_string();

    let mut commits = vec![];
    let mut curr_commit = None;

    for line in str.lines() {
        // Ignore empty lines
        if line.trim().is_empty() {
            continue;
        }

        // Start new commit
        if line.starts_with("commit ") {
            if let Some(c) = curr_commit {
                commits.push(c);
            }

            curr_commit = Some(CommitMetadata {
                hash: line.split(' ').collect::<Vec<&str>>()[1].to_string(),
                date: 0,
                author_name: "".to_string(),
                author_mail: "".to_string(),
                comment: "".to_string(),
            });
        }

        // Parse author name & email
        else if let Some(c) = &mut curr_commit {
            if line.starts_with("Author:") {
                let split = line.split(": ").collect::<Vec<&str>>()[1].to_string();
                let split = split.split(" <").collect::<Vec<&str>>();

                let name = split[0].to_string();
                let email = split[1].replace('>', "");

                c.author_name = name;
                c.author_mail = email;
            }

            // Parse date
            else if line.starts_with("Date: ") {
                let line = (&line[5..]).trim();
                let date = line.parse::<u64>()?;

                c.date = date;
            }

            // Comment line
            else if line.starts_with("    ") {
                if !c.comment.is_empty() {
                    c.comment.push('\n');
                }

                c.comment.push_str(line.trim());
            }
        }
    }

    if let Some(c) = curr_commit {
        commits.push(c);
    }

    Ok(commits)
}

/// Find the commit associated with an object id
pub fn find_commit_by_object_id(path: &str, id: &str) -> Result<CommitMetadata, Box<dyn Error>> {
    let mut cmd = start_cmd(path);
    let output = cmd.arg("log")
        .arg("--date=unix")
        .arg("--find-object")
        .arg(id)
        .output()?;

    Ok(parse_commits_metadata_output(&output)?
        .pop()
        .ok_or_else(|| Box::new(std::io::Error::new(
            ErrorKind::Other, "Could not get commit information !")))?)
}

/// Get the last commit of a repository
pub fn get_last_commit(path: &str) -> Result<CommitMetadata, Box<dyn Error>> {
    let mut cmd = start_cmd(path);
    let output = cmd.arg("log")
        .arg("--date=unix")
        .arg("-n")
        .arg("1")
        .output()?;

    Ok(parse_commits_metadata_output(&output)?
        .pop()
        .ok_or_else(|| Box::new(std::io::Error::new(
            ErrorKind::Other, "Could not get commit information !")))?)
}

/// Get the last commit of a repository for a specific point of the tree
pub fn get_last_commit_for_tree(repo_path: &str, rev: &str, tree_path: &str) -> Result<CommitMetadata, Box<dyn Error>> {
    let mut cmd = start_cmd(repo_path);
    let output = cmd.arg("log")
        .arg("--date=unix")
        .arg("-n")
        .arg("1")
        .arg(rev)
        .arg("--")
        .arg(tree_path)
        .output()?;

    Ok(parse_commits_metadata_output(&output)?
        .pop()
        .ok_or_else(|| Box::new(std::io::Error::new(
            ErrorKind::Other,
            format!(
                "Could not get commit information ! repo path: {} / rev: {} / path: {}\nOutput: {:#?}",
                repo_path,
                rev,
                tree_path,
                output
            ),
        )))?)
}

/// Get a list of commit from a repository
pub fn get_commits_list(path: &str, rev: &str, start: u64, limit: u64) -> Result<Vec<CommitMetadata>, Box<dyn Error>> {
    let mut cmd = start_cmd(path);
    let output = cmd.arg("log")
        .arg(rev)
        .arg("--date=unix")
        .arg("-n")
        .arg(limit.to_string())
        .arg("--skip")
        .arg(start.to_string())
        .output()?;

    parse_commits_metadata_output(&output)
}

/// Get the list of commits targeting a file
pub fn get_file_history(path: &str, revision: &str, file: &str) -> Result<Vec<CommitMetadata>, Box<dyn Error>> {
    let mut cmd = start_cmd(path);
    let output = cmd.arg("log")
        .arg("--date=unix")
        .arg(revision)
        .arg("--")
        .arg(file)
        .output()?;

    parse_commits_metadata_output(&output)
}

/// Get file mode, included as last integer of the string
fn get_file_mode(line: &str) -> Result<u64, ParseIntError> {
    line.split(' ').last().unwrap().parse::<u64>()
}

/// Get file path in full commit line (warning: this works only for commits header with two words)
fn get_file_path(line: &str) -> String {
    line.splitn(3, ' ').last().unwrap().to_string()
}

/// Split diff line numbers
fn split_diff_line_numbers(numbers: &str) -> Result<(u64, u64), ParseIntError> {
    let numbers = numbers.replace('+', "").replace('-', "");

    if numbers.contains(',') {
        let split = numbers.split(',').collect::<Vec<&str>>();
        Ok((split[0].parse::<u64>()?, split[1].parse::<u64>()?))
    } else {
        Ok((numbers.parse::<u64>()?, 0))
    }
}

/// Retrieve all the information about a commit
pub fn get_full_commit(path: &str, hash: &str) -> Result<FullCommit, Box<dyn Error>> {
    let mut cmd = start_cmd(path);
    let output = cmd.arg("show")
        .arg("--date=unix")
        .arg("--full-index")
        .arg(hash)
        .output()?;

    let output_str = String::from_utf8_lossy(&output.stdout).to_string();
    let commit_metadata = parse_commits_metadata_output(&output)?
        .pop()
        .ok_or_else(|| std::io::Error::new(ErrorKind::BrokenPipe, "Failed to parse commit metadata"))?;

    let mut commit = FullCommit { metadata: commit_metadata, diffs: vec![] };

    // Parse diff section
    let mut curr_diff: Option<DiffFile> = None;
    let mut curr_diff_section: Option<DiffFileSection> = None;

    for line in output_str.lines() {

        // Skip commit metadata
        if curr_diff.is_none() && !line.starts_with("diff") {
            continue;
        }

        // Start new diff
        if line.starts_with("diff --git") {
            if let Some(d) = &mut curr_diff {
                if let Some(section) = &curr_diff_section {
                    d.diff_sections.push(section.clone());
                    curr_diff_section = None;
                }


                commit.diffs.push(d.clone())
            }

            let split = line.split(' ').collect::<Vec<&str>>();

            curr_diff = Some(DiffFile {
                source: split[2].replacen("a/", "", 1).to_string(),
                dest: split[3].replacen("b/", "", 1).to_string(),
                extended_headers: vec![],
                diff_sections: vec![],
                is_binary: false,
            });
        }

        // Parse parts of the commit
        else if let Some(d) = &mut curr_diff
        {
            // Headers
            if line.starts_with("old mode")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::OldMode(get_file_mode(line)?));
            } else if line.starts_with("new mode")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::NewMode(get_file_mode(line)?));
            } else if line.starts_with("deleted file mode")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::DeletedFileMode(get_file_mode(line)?));
            } else if line.starts_with("new file mode")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::NewFileMode(get_file_mode(line)?));
            } else if line.starts_with("copy from")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::CopyFrom(get_file_path(line)));
            } else if line.starts_with("copy to")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::CopyTo(get_file_path(line)));
            } else if line.starts_with("rename from")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::RenameFrom(get_file_path(line)));
            } else if line.starts_with("rename to")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::RenameTo(get_file_path(line)));
            } else if line.starts_with("similarity index")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::SimilarityIndex(
                        line.split(' ').last().unwrap().replace('%', "").parse::<u64>()?
                    )
                );
            } else if line.starts_with("dissimilarity index")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::DissimilarityIndex(
                        line.split(' ').last().unwrap().replace('%', "").parse::<u64>()?
                    )
                );
            } else if line.starts_with("copy from")
            {
                d.extended_headers.push(
                    ExtendedDiffHeader::CopyFrom(get_file_path(line)));
            } else if line.starts_with("index")
            {
                let split = line.split(' ').collect::<Vec<&str>>();
                let hash_split = split[1].split("..").collect::<Vec<&str>>();

                d.extended_headers.push(
                    ExtendedDiffHeader::Index(DiffIndexHeader {
                        hash1: hash_split[0].to_string(),
                        hash2: hash_split[1].to_string(),
                        mode: get_file_mode(line).ok(),
                    }));
            }

            // Binary files
            else if line.starts_with("Binary files") {
                d.is_binary = true;
            }


            // Ignored headers
            else if curr_diff_section.is_none() && (line.starts_with("--- ") || line.starts_with("+++ "))
            {
                continue;
            }

            // Diff sections
            else if line.starts_with("@@") {
                if let Some(section) = &curr_diff_section {
                    d.diff_sections.push(section.clone())
                }

                let split = line.split(' ').collect::<Vec<&str>>();
                let old = split_diff_line_numbers(split[1])?;
                let new = split_diff_line_numbers(split[2])?;

                curr_diff_section = Some(DiffFileSection {
                    old_start_line: old.0,
                    old_lines_count: old.1,
                    new_start_line: new.0,
                    new_lines_count: new.1,
                    diff: "".to_string(),
                });
            }

            // Diff file
            else if let Some(s) = &mut curr_diff_section {
                if line.starts_with('+') || line.starts_with('-') || line.starts_with(' ')
                {
                    s.diff.push_str(line);
                    s.diff.push('\n');
                }
            }
        }
    }

    if let Some(d) = &mut curr_diff {
        if let Some(section) = curr_diff_section {
            d.diff_sections.push(section)
        }

        commit.diffs.push(d.clone())
    }

    Ok(commit)
}

/// Check out whether a given file is a binary file or not
///
/// Try to read the first bytes of the file and parse them in UTF-8
#[allow(unused_must_use)]
pub fn is_file_binary(repo: &str, rev: &str, file_path: &str) -> Result<bool, Box<dyn Error>>
{
    let mut cmd = start_cmd(repo);
    cmd.arg("show")
        .arg(format!("{}:{}", rev, file_path))
        .stdout(Stdio::piped());

    let mut buff = [0; 100];

    let mut child = cmd.spawn()?;
    let out = child.stdout.as_mut().unwrap();
    let count = out.read(&mut buff)?;

    child.kill();
    child.wait()?;

    Ok(String::from_utf8(Vec::from(&buff[0..count])).is_err())
}
extern crate yaml_rust;

use std::{fs, process};
use std::io::Write;
use std::path::Path;

use futures::io::ErrorKind;
use yaml_rust::YamlLoader;

use simple_git_server::git::is_git_dir;
use simple_git_server::server::{MultiRepositoriesServer, start_multiple_repo_server, start_single_repo_server};

/// Simple Git server
///
/// @author Pierre Hubert
#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    println!("Simple Read Only Git Server (SROGS)");
    println!("(c) Pierre Hubert 2020");

    // Get the path to the repository
    let mut args: Vec<String> = std::env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage / Crypt password: {} crypt_pass", args[0]);
        eprintln!("Usage / Basic server: {} <path-to-git-dir>", args[0]);
        eprintln!("Usage / Multi repositories server: {} --multi <path-to-conf>", args[0]);
        process::exit(1);
    }

    // Crypt password
    if args[1] == "crypt_pass"
    {
        crypt_password()?;
        return Ok(());
    }

    // Check which server we should start
    match args.len() {
        2 => start_basic_server(&mut args).await,
        _ => start_multi_server(&args).await
    }
}

/// Crypt password
fn crypt_password() -> std::io::Result<()>
{
    print!("Password to crypt: ");
    let mut buffer = String::new();
    std::io::stdout().flush()?;
    std::io::stdin().read_line(&mut buffer)?;

    // Remove trailing line (if any)
    buffer = buffer.split('\n').next().unwrap().to_string();

    println!("Clear password: {}", buffer);
    println!("Password hash: {}", bcrypt::hash(buffer, bcrypt::DEFAULT_COST)
        .expect("Failed to crypt password ! "));

    Ok(())
}

/// Start basic server
async fn start_basic_server(args: &mut [String]) -> std::io::Result<()> {
    // Port number is currently hard coded
    let port = 2356;

    // Check git repository
    let dir = &mut args[1];
    if !dir.ends_with('/') {
        dir.push('/')
    }
    if !is_git_dir(dir) {
        eprintln!("{} does not seems to be a valid git repository!", &dir);
        process::exit(2);
    }


    // Start server
    start_single_repo_server(port, dir).await
}

/// Start server with configuration
async fn start_multi_server(args: &[String]) -> std::io::Result<()> {
    if args.len() != 3 {
        eprintln!("Invalid number of arguments!");
        process::exit(2);
    }


    // Read configuration
    let config_content = fs::read_to_string(&args[2])?;

    // Parse configuration
    let conf = YamlLoader::load_from_str(&config_content)
        .map_err(|_| std::io::Error::new(ErrorKind::InvalidData, "could not parse yaml!"))?;
    if conf.len() != 1 {
        panic!("conf.len() != 1!");
    }
    let conf = &conf[0];

    // Load configuration
    let server = MultiRepositoriesServer {
        root_path: conf["path"].as_str().expect("path missing!").to_string(),

        port: conf["port"].as_i64().expect("port missing!") as u16,
        hostname: conf["hostname"].as_str().expect("hostname missing!").to_string(),

        username: conf["username"].as_str().expect("username missing!").to_string(),
        password: conf["password"].as_str().expect("password missing!").to_string(),

        allow_create_on_push: conf["allow_create_on_push"].as_bool().unwrap_or(false),

        access_control_allow_origin: conf["access_control_allow_origin"].as_str().map(|f| f.to_string()),

        front_end_files: conf["front_end_files"].as_str().map(|f| f.to_string()),
    };

    // Check if given path exists
    if !Path::new(&server.root_path).is_dir() {
        eprintln!("Given path in configuration \"{}\" is not a directory!", &server.root_path);
        process::exit(3);
    }

    start_multiple_repo_server(server).await
}
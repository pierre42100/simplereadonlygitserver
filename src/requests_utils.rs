//! # Requests utilities
//!
//! @author Pierre Hubert

/// Check request hash (commits, objects, ...)
///
/// ```
/// use simple_git_server::requests_utils::check_req_hash;
/// assert_eq!(check_req_hash("a151"), true);
/// assert_eq!(check_req_hash("a151 "), false);
/// assert_eq!(check_req_hash(""), false);
/// assert_eq!(check_req_hash("é"), false);
/// ```
pub fn check_req_hash(hash: &str) -> bool {
    if hash.is_empty() {
        return false;
    }

    let re = regex::Regex::new(r"^[a-z0-9]+$").unwrap();
    re.is_match(hash)
}

/// Check request hash (commits, objects, ...)
///
/// ```
/// use simple_git_server::requests_utils::check_req_hash;
/// assert_eq!(check_req_hash(" "), false);
/// assert_eq!(check_req_hash("a151 "), false);
/// assert_eq!(check_req_hash("a/151"), false);
/// assert_eq!(check_req_hash(""), false);
/// assert_eq!(check_req_hash("e"), true);
/// ```
pub fn check_req_rev(hash: &str) -> bool {
    if hash.is_empty()
        || hash.contains('/')
        || hash.contains(' ') {
        return false;
    }

    true
}


/// Decode percents included in URLs
///
/// ```
/// use simple_git_server::requests_utils::url_decode_percents;
///
/// assert_eq!(" ", url_decode_percents("%20"));
/// assert_eq!(" a", url_decode_percents("%20a"));
/// assert_eq!("a", url_decode_percents("a"));
/// ```
pub fn url_decode_percents(str: &str) -> String
{
    return percent_encoding::percent_decode(str.as_bytes()).decode_utf8_lossy().to_string();
}
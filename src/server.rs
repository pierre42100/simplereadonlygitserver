use std::borrow::Cow;
use std::collections::HashMap;
use std::io::{Read, Write};
use std::ops::Add;
use std::path::Path;
use std::pin::Pin;
use std::process::Child;
use std::str::Utf8Error;
use std::sync::Arc;

use actix_web::{App, Error, error, FromRequest, HttpRequest, HttpResponse, HttpResponseBuilder, HttpServer, Responder, web};
use actix_web::dev::{Decompress, Payload};
use actix_web::error::ErrorInternalServerError;
use actix_web::web::{Bytes, BytesMut};
use futures::{Stream, StreamExt};
use futures::future::{ok, Ready};
use futures::task::{Context, Poll};

use crate::git::*;
use crate::requests_utils::{check_req_hash, check_req_rev, url_decode_percents};
use crate::stats::get_dir_size;
use crate::structures::{ObjectsToCommitsMappingResponse, RepoStats, TreeEntryWithCommit};

/// Max normal requests size
const MAX_SIZE: usize = 262_144; // max payload size is 256k

pub struct MultiRepositoriesServer {
    pub root_path: String,
    pub port: u16,
    pub hostname: String,
    pub username: String,
    pub password: String,
    pub allow_create_on_push: bool,
    pub access_control_allow_origin: Option<String>,
    pub front_end_files: Option<String>,
}


struct Server {
    path: String,
    username: Option<String>,
    password: Option<String>,
    allow_create_on_push: bool,
    access_control_allow_origin: Option<String>,
    front_end_files: Option<String>,
}

/// Custom extractor
struct CustomRequest {
    req: HttpRequest,
    srv: Arc<Server>,
    stream: Decompress<Payload>,

    // Helpers for multiple repositories mode
    computed_repo_path: Option<String>,
    computed_uri: Option<String>,
    base_uri: Option<String>,
}

impl CustomRequest {
    fn get_uri(&self) -> &str {
        match &self.computed_uri {
            Some(s) => s,
            None => self.req.path()
        }
    }

    /// Get repository path
    fn get_path(&self) -> &str {
        match &self.computed_repo_path {
            Some(s) => s,
            None => &self.srv.path
        }
    }

    fn get_base_uri(&self) -> &str {
        match &self.base_uri {
            Some(s) => s,
            None => "/"
        }
    }

    fn has_frontend(&self) -> bool
    {
        self.srv.front_end_files.is_some()
    }

    /// Read entire request body into memory
    async fn read_all_body(&mut self) -> actix_web::Result<BytesMut> {
        // payload is a stream of Bytes objects
        let mut body = BytesMut::new();
        while let Some(chunk) = self.stream.next().await {
            let chunk = chunk?;
            // limit max size of in-memory payload
            if (body.len() + chunk.len()) > MAX_SIZE {
                return Err(error::ErrorBadRequest("overflow"));
            }
            body.extend_from_slice(&chunk);
        }

        Ok(body)
    }
}


impl FromRequest for CustomRequest {
    type Error = Error;
    type Future = Ready<Result<CustomRequest, Error>>;

    #[inline]
    fn from_request(req: &HttpRequest, _payload: &mut Payload) -> Self::Future {
        ok(CustomRequest {
            req: req.clone(),
            srv: Arc::clone(req.app_data().unwrap()),
            stream: Decompress::from_headers(_payload.take(), req.headers()), // Do not worry if your IDE report an error here

            computed_repo_path: None,
            computed_uri: None,
            base_uri: None,
        })
    }
}


/// Stream stdout of a process to request response
struct Uploader {
    stdout: Child,
    begin_with: Option<Vec<u8>>,
}

impl Stream for Uploader {
    type Item = Result<Bytes, Error>;

    fn poll_next(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {

        // Check if we have some content to start with first
        if self.begin_with.is_some() {
            return Poll::Ready(Some(Ok(Bytes::from(self.begin_with.take().unwrap()))));
        }

        if self.stdout.stdout.is_none()
        {
            eprintln!("Can not read from inner process !");
            return Poll::Ready(None);
        }

        let mut buf: [u8; 10000] = [0; 10000];
        let res = self.stdout.stdout.as_mut().unwrap().read(&mut buf[..]);

        match res {
            Ok(0) => {
                if let Err(e) = self.stdout.wait()
                {
                    eprintln!("Failed to wait for process termination! Error: {}", e);
                }
                Poll::Ready(None)
            }

            Ok(res) => Poll::Ready(Some(Ok(Bytes::from(Vec::from(&buf[..res]))))),

            Err(e) => {
                eprintln!("upload error {}", e);
                Poll::Ready(None)
            }
        }
    }
}

async fn upload_stream_with_begin(stdout: Child,
                                  content_type: &str,
                                  begin_with: Option<&[u8]>,
                                  initial_headers: Option<HashMap<String, String>>) -> actix_web::Result<HttpResponse> {
    let mut response = HttpResponse::Ok();
    response.insert_header(("Content-Type", content_type));

    if let Some(headers) = initial_headers
    {
        for (k, v) in headers
        {
            response.insert_header((k.as_str(), v));
        }
    }

    let stream = Uploader {
        stdout,
        begin_with: begin_with.map(Vec::from),
    };

    Ok(response.streaming(stream))
}

async fn upload_stream(stdout: Child, content_type: &str, initial_headers: Option<HashMap<String, String>>) -> actix_web::Result<HttpResponse> {
    upload_stream_with_begin(stdout, content_type, None, initial_headers).await
}

/// Start request response
fn start_response(req: &CustomRequest) -> HttpResponseBuilder
{
    let mut res = HttpResponse::Ok();

    if let Some(access_control) = &req.srv.access_control_allow_origin
    {
        res.insert_header(("Access-Control-Allow-Origin", access_control.as_str()));
    }

    res
}

/// Decode input path
fn uri_decode(input: &str) -> Result<Cow<str>, Utf8Error> {
    let path = percent_encoding::percent_decode_str(input);
    path.decode_utf8()
}

/// Serve front end file
async fn serve_front_end(req: &CustomRequest, mut is_not_found: bool) -> actix_web::Result<HttpResponse>
{
    let mut file = None;

    let frontend_path = &req.srv.front_end_files.as_ref().unwrap().to_string();

    // Attempt to read file
    if req.req.uri() != "/"
    {
        file = std::fs::read(format!("{}{}", &frontend_path, req.req.uri())).ok()
    }

    // Fallback to index file if file is not found
    if file.is_none()
    {
        file = std::fs::read(format!("{}/index.html", &frontend_path)).ok();
    }

    // The file is an asset, it is mandatory : we found it
    else {
        is_not_found = false;
    }

    if file.is_none()
    {
        return Ok(HttpResponse::InternalServerError().body("Fronted index not found !"));
    }

    let file = file.unwrap();

    if is_not_found
    {
        Ok(HttpResponse::NotFound().body(file))
    } else {
        Ok(HttpResponse::Ok().body(file))
    }
}

/// Browse the list of files
async fn browse(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    if req.has_frontend()
    {
        return serve_front_end(&req, true).await;
    }

    // Extract requested path & revison
    let mut path = req.get_uri().to_string();
    if path.starts_with("/tree") {
        path = path.replacen("/tree", "", 1);
    }

    if path.starts_with('/') {
        path = path[1..].to_string();
    }

    let mut path_split = path.splitn(2, '/').collect::<Vec<&str>>();

    let revision = match path.is_empty() {
        true => default_branch(req.get_path())?,
        false => path_split.first().unwrap().to_string()
    };

    if !check_req_rev(&revision)
    {
        return Ok(bad_request(req.req).await);
    }

    path_split.remove(0);
    let mut path = path_split.join("/");

    if !path.ends_with('/') && path.len() > 1 {
        path.push('/');
    }


    // Decode path
    let mut path = uri_decode(&path).map_err(|_|
        ErrorInternalServerError("decode path")
    )?.to_string();

    // Ensure directory listing will be forced
    if !path.ends_with('/') {
        path.push('/');
    }

    let path = &path[..];

    // List the files in the repository
    let list = list_files(req.get_path(), path, &revision)?;


    if list.is_empty() {
        return Ok(HttpResponse::NotFound()
            .insert_header(("Content-Type", "text/plain"))
            .body("404 file not found"));
    }


    let mut response = String::from_utf8_lossy(include_bytes!("../templates/files_list.html")).to_string();
    response = response.replace("{dir}", path);


    // Remove first slash for file
    let path = &path[1..];

    let mut files_list = String::new();

    if !path.is_empty() {
        files_list.push_str("<li><a href='../'>..</a></li>");
    }

    for file in list {
        let entry = if file.is_dir {
            format!("<li><a href='{{base_uri}}tree/{}/{}'>{}</a></li>", revision, file.file_name, file.file_name.rsplit('/').next().unwrap())
        } else {
            format!("<li><a href='{{base_uri}}raw/{}/{}'>{}</a></li>", revision, file.file_name, file.file_name.rsplit('/').next().unwrap())
        };
        files_list.push_str(&entry);
    }

    response = response.replace("{files_list}", &files_list);

    // Fix paths
    response = response.replace("{base_uri}", req.get_base_uri());
    response = response.replace("{revision}", &revision);

    let branch_formatter = |el: &String| format!("<option value=\"{}\" {}>{}</option>", el, match el.eq(&revision) {
        true => "selected=\"\"",
        false => ""
    }, el);

    let branches_options = list_branches(req.get_path())?;
    let branches_options = branches_options.iter()
        .map(branch_formatter)
        .collect::<Vec<String>>()
        .join("\n");

    let tags_options = list_tags(req.get_path())?
        .iter()
        .map(branch_formatter)
        .collect::<Vec<String>>()
        .join("\n");

    response = response.replace("{branches_options}", &branches_options);
    response = response.replace("{tags_options}", &tags_options);

    Ok(HttpResponse::Ok().insert_header(("Content-Type", "text/html")).body(response))
}

/// Get raw file content
async fn raw(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let path = String::from(req.get_uri()).replacen("/raw/", "", 1);

    let path_split = path.splitn(2, '/').collect::<Vec<&str>>();

    if path_split.len() != 2 {
        return Ok(bad_request(req.req).await);
    }

    let revision = path_split[0];
    let path = uri_decode(path_split[1])?;

    if !file_exists(req.get_path(), revision, &path)? {
        return Err(error::ErrorNotFound("404 not found"));
    }

    let mut initial_headers = HashMap::new();

    if req.req.query_string().contains("download")
    {
        initial_headers.insert("Content-Disposition".to_string(), format!("attachment; filename={}", path.split('/').last().unwrap()));
    }

    if let Some(access_control) = &req.srv.access_control_allow_origin
    {
        initial_headers.insert("Access-Control-Allow-Origin".to_string(), access_control.to_string());
    }

    upload_stream(get_file_content(req.get_path(), revision, &path)?, "", Some(initial_headers)).await
}

/// Download repository as an archive
async fn archive(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let (format, mime_type) = if req.get_uri().contains(".zip") {
        ("zip", "application/zip")
    } else {
        ("tar", "application/x-tar")
    };

    let revision = req
        .get_uri()
        .rsplit('/')
        .next()
        .unwrap()
        .rsplitn(2, '.')
        .last()
        .unwrap();

    upload_stream(archive_repo(req.get_path(), revision, format)?, mime_type, None).await
}

/// Bad request
async fn bad_request(req: HttpRequest) -> HttpResponse {
    println!("400 {:?}", req.uri().path_and_query());
    HttpResponse::BadRequest().body("400 Bad request".to_string())
}

/// Default 404 not found route
async fn not_found(req: HttpRequest) -> HttpResponse {
    println!("404 {:?}", req.uri().path_and_query());
    HttpResponse::NotFound().body("404 Resource not found".to_string())
}

/// Validate user credentials
///
/// Returns None in case of success, otherwise it returns an error
fn check_auth(req: &CustomRequest) -> Option<HttpResponse> {

    // Do this only if there are credentials available
    if req.srv.username.is_none() || req.srv.password.is_none() {
        return None;
    }

    // Extract from authorization header username & password
    if !req.req.headers().contains_key("authorization") {
        return Some(HttpResponse::Unauthorized()
            .insert_header(("Www-Authenticate", "Basic realm=\".\"")).body("Auth required"));
    }
    let auth_header = req.req.headers().get("authorization").unwrap().to_str().unwrap().to_string();
    let auth_credentials = auth_header.split(' ').last().unwrap();

    let decoded = base64::decode(auth_credentials).unwrap_or_default();
    let decoded = String::from_utf8_lossy(&decoded);
    let split: Vec<&str> = decoded.split(':').collect();

    if split.len() != 2
    {
        eprintln!("Failed to authenticate user : malformed authorization header !");
        return Some(HttpResponse::BadRequest().body("Bad auth"));
    }

    let username = split[0].to_string();
    let password = split[1].to_string();

    // Validate the credentials
    if !username.eq(req.srv.username.as_ref().unwrap()) ||
        !bcrypt::verify(password, req.srv.password.as_ref().unwrap()).unwrap_or(false)
    {
        return Some(HttpResponse::Unauthorized()
            .insert_header(("Www-Authenticate", "Basic realm=\".\"")).body("Auth required"));
    }

    None
}

/// To upload pack (git clone / git pull)
async fn upload_pack_refs(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let begin_content = "001e# service=git-upload-pack\n0000".as_bytes();

    let stdout = get_upload_refs(req.get_path())?;

    upload_stream_with_begin(stdout,
                             "application/x-git-upload-pack-advertisement",
                             Some(begin_content),
                             None,
    ).await
}

/// To receive pack (git push)
async fn receive_pack_refs(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    if let Some(e) = check_auth(&req) {
        return Ok(e);
    }

    let begin_content = "001f# service=git-receive-pack\n0000".as_bytes();

    let stdout = get_receive_refs(req.get_path())?;

    upload_stream_with_begin(stdout,
                             "application/x-git-receive-pack-advertisement",
                             Some(begin_content),
                             None,
    ).await
}

/// Get git references
async fn git_refs(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    match req.req.query_string() {

        // git pull
        "service=git-upload-pack" => upload_pack_refs(req).await,

        // git push
        "service=git-receive-pack" => receive_pack_refs(req).await,

        _ => {
            println!("Error 500 - git_refs");
            Err(error::ErrorNotFound("not found"))
        }
    }
}


/// Git pull / clone => upload pack
async fn upload_pack(mut req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let mut bytes = BytesMut::new();
    while let Some(item) = req.stream.next().await {
        bytes.extend_from_slice(&item.map_err(
            |_| ErrorInternalServerError("internal error"))?);
    }

    let stdout = git_upload_pack(req.get_path(), bytes.as_ref())?;

    upload_stream(stdout, "application/x-git-upload-pack-result", None).await
}

/// Git push => receive pack
async fn receive_pack(mut req: CustomRequest) -> actix_web::Result<HttpResponse> {
    if let Some(e) = check_auth(&req) {
        return Ok(e);
    }

    let mut child = git_receive_pack(req.get_path())?;

    let stdin = child.stdin.as_mut().ok_or_else(
        || ErrorInternalServerError("internal error - get stdin"))?;

    // Write input
    while let Some(item) = req.stream.next().await {
        let chunk = &item.map_err(|_| ErrorInternalServerError("internal error"))?;

        if stdin.write(chunk.as_ref())? != chunk.len() {
            return Err(ErrorInternalServerError("internal error - incomplete write"));
        }
    }

    upload_stream(child, "", None).await
}

/// Start the server for a given repository
pub async fn start_single_repo_server(port: u16, path: &str) -> std::io::Result<()> {
    let listen_addr = format!("0.0.0.0:{}", port);
    println!("Server is starting on http://{}/", listen_addr);

    let path = path.to_string();

    HttpServer::new(move || {
        App::new()
            .app_data(Arc::new(Server {
                path: path.to_string(),
                username: None,
                password: None,
                allow_create_on_push: false,
                access_control_allow_origin: None,
                front_end_files: None,
            }))

            // Route requests
            .route("**", web::get().to(route_req))
            .route("**", web::post().to(route_req))
    }).bind(listen_addr)?.run().await
}


/// List repositories in multi mode
async fn list_repositories(req: CustomRequest) -> actix_web::Result<impl Responder> {
    if req.has_frontend()
    {
        return serve_front_end(&req, false).await;
    }

    let mut list_repos = String::new();

    for repo in list_git_repos(&req.srv.path)? {
        let content = format!("<li><a href=\"{}\">{}</a></li>\n", &repo, &repo);
        list_repos = list_repos.add(&content);
    }


    let content = String::from_utf8_lossy(include_bytes!("../templates/repos_list.html"));

    Ok(HttpResponse::Ok()
        .insert_header(("Content-Type", "text/html"))
        .body(content.replace("{repos_list}", &list_repos))
    )
}

/// API listing of repositories in multi mode
async fn api_list_repositories(req: CustomRequest) -> actix_web::Result<impl Responder> {
    let list = list_git_repos(&req.srv.path)?;

    Ok(start_response(&req).json(list))
}

/// API listing of available branches in repo
async fn api_list_branches(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let list = list_branches(req.get_path())?;

    Ok(start_response(&req).json(list))
}

/// API get default branch
async fn api_default_branch(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let list = default_branch(req.get_path())?;

    Ok(start_response(&req).json(list))
}

/// API listing of available tags in repo
async fn api_list_tags(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let list = list_tags(req.get_path())?;

    Ok(start_response(&req).json(list))
}

/// API tree listing
async fn api_list_tree(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let uri = req.get_uri().replacen("/api/tree/", "", 1);
    let uri_split = uri.splitn(2, '/').collect::<Vec<&str>>();

    if uri_split.len() != 2 {
        return Ok(bad_request(req.req).await);
    }

    let revision = uri_split[0];
    let path = url_decode_percents(&format!("/{}", uri_split[1]));

    if !check_req_rev(revision)
    {
        return Ok(bad_request(req.req).await);
    }

    let list = list_files(req.get_path(), &path, revision)?;
    Ok(start_response(&req).json(list))
}

/// API tree listing with commits
async fn api_list_tree_with_commits(req: CustomRequest) -> actix_web::Result<HttpResponse>
{
    let uri = req.get_uri().replacen("/api/tree_with_commits/", "", 1);
    let uri_split = uri.splitn(2, '/').collect::<Vec<&str>>();

    if uri_split.len() != 2 {
        return Ok(bad_request(req.req).await);
    }

    let revision = uri_split[0];
    let path = url_decode_percents(&format!("/{}", uri_split[1]));

    if !check_req_rev(revision)
    {
        return Ok(bad_request(req.req).await);
    }

    let list = list_files(req.get_path(), &path, revision)?;

    let mut result = vec![];

    for file in list
    {
        result.push(TreeEntryWithCommit {
            commit: get_last_commit_for_tree(req.get_path(), revision, &file.file_name).unwrap(),
            entry: file,
        });
    }

    Ok(start_response(&req).json(result))
}

/// Get the commit associated with an object
async fn api_get_commit_for_object(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let hash: String = req.get_uri().split('/').collect::<Vec<&str>>()[3].to_string();

    if hash.is_empty() || !check_req_hash(&hash) {
        return Ok(bad_request(req.req).await);
    }

    let commit = find_commit_by_object_id(req.get_path(), &hash)
        .map_err(|_| ErrorInternalServerError("Could not find commit!"))?;

    Ok(start_response(&req).json(commit))
}

/// Get the commits for a given list of objects
async fn api_get_commits_for_objects(mut req: CustomRequest) -> actix_web::Result<HttpResponse> {
    // Process tags
    let obj = serde_json::from_slice::<Vec<String>>(&req.read_all_body().await?)?;

    let mut commits = vec![];
    let mut mapping = HashMap::new();

    for hash in obj {
        if !check_req_hash(&hash) {
            return Ok(bad_request(req.req).await);
        }

        let commit = find_commit_by_object_id(req.get_path(), &hash);

        if let Ok(commit) = commit
        {
            mapping.insert(hash, commit.hash.to_string());

            if !commits.contains(&commit)
            {
                commits.push(commit);
            }
        } else {
            eprintln!("Failed to fetch commit for object {}", hash);
        }
    }


    Ok(start_response(&req).json(ObjectsToCommitsMappingResponse { commits, mapping }))
}

/// Reroute requests
async fn reroute_requests_for_multi_repo_mode(mut req: CustomRequest) -> actix_web::Result<HttpResponse> {

    // Check for create on push
    if req.srv.allow_create_on_push && req.req.path().ends_with("/info/refs")
        && req.req.query_string().eq("service=git-receive-pack") {

        // Check auth
        if let Some(e) = check_auth(&req) {
            return Ok(e);
        }

        // Get the path of the repository
        let path = &req.req.path();
        let path = &path[..path.len() - "/info/refs".len()];
        let path = Path::new(&req.srv.path).join(&path[1..]);
        let path = path.to_str().ok_or_else(
            || ErrorInternalServerError("could not join paths for auto-create"))?;

        if !Path::new(path).exists() {
            create_bare_repo(path)?;
        }
    }


    // Extract right repository information
    let list = list_git_repos(&req.srv.path)?;

    // Search the repo
    let req_path = &(&req.req).path()[1..];
    let repo = list.iter()
        .find(|entry|
            req_path.eq(&entry[..]) ||
                req_path.eq(&format!("{}.git", &entry[..])) ||
                req_path.starts_with(&format!("{}/", &entry[..])) ||
                req_path.starts_with(&format!("{}.git/", &entry[..]))
        );


    if repo.is_none() && req.has_frontend()
    {
        return serve_front_end(&req, true).await;
    }

    let repo = repo.ok_or_else(|| error::ErrorNotFound("404 repository not found"))?;

    // Get repo path
    let repo_path = Path::new(&req.srv.path).join(repo);
    let repo_path = repo_path.to_str().ok_or_else(
        || ErrorInternalServerError("repo_path could not be determined!")
    )?;

    // Extract request uri
    let uri = match &req.req.path()[1 + repo.len()..] {
        "" => "/",
        ".git" => "/",

        // We remove the .git (if present)
        s => &s[s.find('/').unwrap()..]
    };

    // Fix request
    req.computed_uri = Some(uri.to_string());
    req.computed_repo_path = Some(format!("{}/", repo_path));
    req.base_uri = Some(format!("/{}/", repo));

    route_req(req).await
}

/// Get the last commit of a repo
async fn api_get_last_commit(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let commit = get_last_commit(req.get_path())
        .map_err(|_| ErrorInternalServerError("Could not find commit!"))?;

    Ok(start_response(&req).json(commit))
}

/// Get the list of commits
async fn api_get_commits_list(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let info = req.get_uri().split("/list/").collect::<Vec<&str>>().last().unwrap().to_string();
    let split = info.split('/').collect::<Vec<&str>>();

    if split.len() != 2
    {
        return Ok(bad_request(req.req).await);
    }

    let rev = split[0];
    let start = split[1].parse::<u64>().unwrap_or(0);

    if !check_req_rev(rev)
    {
        return Ok(bad_request(req.req).await);
    }

    let commits = get_commits_list(req.get_path(), rev, start, 50)
        .map_err(|_| ErrorInternalServerError("Could not get the list of commits!"))?;

    Ok(start_response(&req).json(commits))
}

/// Get full information about a commit
async fn api_get_full_commit(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let hash: String = req.get_uri().split('/').collect::<Vec<&str>>()[4].to_string();

    if !check_req_hash(&hash) {
        return Ok(bad_request(req.req).await);
    }

    let commit = get_full_commit(req.get_path(), &hash)
        .map_err(|_| ErrorInternalServerError("Could not find commit!"))?;

    Ok(start_response(&req).json(commit))
}

/// Get the parent of a commit
async fn api_get_parent_commit(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let hash: String = req.get_uri().split('/').last().unwrap().to_string();

    if !check_req_hash(&hash) {
        return Ok(bad_request(req.req).await);
    }

    let mut commits = get_commits_list(req.get_path(), &hash, 1, 1)
        .map_err(|_| ErrorInternalServerError("Could not find commit!"))?;

    if commits.is_empty()
    {
        return Ok(not_found(req.req).await);
    }

    Ok(start_response(&req).json(commits.remove(0)))
}

/// Get file history
async fn api_get_file_history(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let split = req
        .get_uri()
        .replacen("/api/history/", "", 1);
    let split = split.splitn(2, '/')
        .collect::<Vec<&str>>();

    if split.len() != 2 {
        return Ok(bad_request(req.req).await);
    }

    let revision = split[0];
    let file = split[1];

    if !check_req_rev(revision) {
        return Ok(bad_request(req.req).await);
    }

    let commit = get_file_history(req.get_path(), revision, file)
        .map_err(|_| ErrorInternalServerError("Could not find commits!"))?;

    Ok(start_response(&req).json(commit))
}

/// Get repo statistics
async fn api_get_repo_stats(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let revision = req.get_uri().replacen("/api/stats/", "", 1);
    if !check_req_rev(&revision)
    {
        return Ok(bad_request(req.req).await);
    }

    let stats = RepoStats {
        size: get_dir_size(req.get_path())?,
        number_commits: count_commits(req.get_path(), &revision)?,
    };

    Ok(start_response(&req).json(stats))
}

/// Check out whether a file a is a binary file or not
async fn api_file_is_binary(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let uri = req.get_uri().replacen("/api/files/is_binary/", "", 1);
    let uri_split = uri.splitn(2, '/').collect::<Vec<&str>>();

    if uri_split.len() != 2 {
        return Ok(bad_request(req.req).await);
    }

    let revision = uri_split[0];
    let file_path = uri_split[1].to_string();

    if !check_req_rev(revision)
    {
        return Ok(bad_request(req.req).await);
    }

    let res = is_file_binary(req.get_path(), revision, &file_path)
        .map_err(|_| ErrorInternalServerError("Could not process request!"))?;

    Ok(start_response(&req).json(res))
}

/// Handle route on repo
async fn route_req(req: CustomRequest) -> actix_web::Result<HttpResponse> {
    let uri = req.get_uri();

    // Common & web routes
    if uri.eq("/") || uri.starts_with("/tree/") {
        browse(req).await
    } else if uri.starts_with("/raw/") {
        raw(req).await
    } else if uri.starts_with("/archive/") {
        archive(req).await
    }


    // API requests
    else if uri.eq("/api/branches") {
        api_list_branches(req).await
    } else if uri.eq("/api/default_branch") {
        api_default_branch(req).await
    } else if uri.eq("/api/tags") {
        api_list_tags(req).await
    } else if uri.starts_with("/api/tree/") {
        api_list_tree(req).await
    } else if uri.starts_with("/api/tree_with_commits/") {
        api_list_tree_with_commits(req).await
    } else if uri.starts_with("/api/object/") && uri.ends_with("/commit") {
        api_get_commit_for_object(req).await
    } else if uri.eq("/api/objects/commits") {
        api_get_commits_for_objects(req).await
    } else if uri.eq("/api/commits/last") {
        api_get_last_commit(req).await
    } else if uri.starts_with("/api/commits/list/") {
        api_get_commits_list(req).await
    } else if uri.starts_with("/api/commits/full/") {
        api_get_full_commit(req).await
    } else if uri.starts_with("/api/commits/parent/") {
        api_get_parent_commit(req).await
    } else if uri.starts_with("/api/history/") {
        api_get_file_history(req).await
    } else if uri.starts_with("/api/stats/") {
        api_get_repo_stats(req).await
    } else if uri.starts_with("/api/files/is_binary/") {
        api_file_is_binary(req).await
    }

    // Git command
    else if uri.eq("/info/refs") {
        git_refs(req).await
    } else if uri.eq("/git-upload-pack") {
        upload_pack(req).await
    } else if uri.eq("/git-receive-pack") {
        receive_pack(req).await
    } else if req.has_frontend() {
        serve_front_end(&req, true).await
    }

    // Error
    else {
        Ok(not_found(req.req).await)
    }
}


/// Start the server for multiple repositories
pub async fn start_multiple_repo_server(conf: MultiRepositoriesServer) -> std::io::Result<()> {
    let listen_addr = format!("{}:{}", conf.hostname, conf.port);
    println!("Server is starting on http://{}/", listen_addr);

    let app_data = Arc::new(Server {
        path: conf.root_path.to_string(),
        username: Some(conf.username),
        password: Some(conf.password),
        allow_create_on_push: conf.allow_create_on_push,
        access_control_allow_origin: conf.access_control_allow_origin,
        front_end_files: conf.front_end_files,
    });

    HttpServer::new(move || {
        App::new()
            .app_data(Arc::clone(&app_data))
            .route("/", web::get().to(list_repositories))
            .route("/api/repos_list", web::get().to(api_list_repositories))
            .route("{tail:.*}", web::get().to(reroute_requests_for_multi_repo_mode))
            .route("{tail:.*}", web::post().to(reroute_requests_for_multi_repo_mode))
    }).bind(listen_addr)?.run().await
}
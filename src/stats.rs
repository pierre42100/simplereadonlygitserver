//! General statistics
//!
//! @author Pierre Hubert

use std::process::Command;

/// Get the size of a directory
pub fn get_dir_size(path: &str) -> std::io::Result<u64>
{
    let cmd = Command::new("du")
        .arg("-s")
        .arg(path)
        .output()?;

    let s = String::from_utf8_lossy(&cmd.stdout);
    let split = s.split('\t').collect::<Vec<&str>>();

    Ok(split[0].parse::<u64>().unwrap_or(0) * 1000)
}
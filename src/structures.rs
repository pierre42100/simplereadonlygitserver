//! Project structures
//!
//! @author Pierre Hubert

use std::collections::HashMap;

use serde::ser::SerializeMap;
use serde::Serialize;
use serde::Serializer;

/// Tree node information
#[derive(Debug)]
pub struct TreeEntry {
    pub is_dir: bool,
    pub size: u64,
    pub object: String,
    pub file_name: String,
}

impl serde::Serialize for TreeEntry {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error> where
        S: Serializer {
        let mut map = serializer.serialize_map(None)?;
        map.serialize_entry("is_dir", &self.is_dir)?;
        map.serialize_entry("size", &self.size)?;
        map.serialize_entry("object", &self.object)?;
        map.serialize_entry("file_name", &self.file_name)?;
        map.end()
    }
}

#[derive(Serialize, Debug)]
pub struct TreeEntryWithCommit {
    pub entry: TreeEntry,
    pub commit: CommitMetadata,
}

/// Basic commit information
#[derive(Serialize, Clone, PartialEq, Debug)]
pub struct CommitMetadata {
    pub hash: String,
    pub date: u64,
    pub author_name: String,
    pub author_mail: String,
    pub comment: String,
}

/// Specific diff section
#[derive(Serialize, Clone)]
pub struct DiffFileSection {
    pub old_start_line: u64,
    pub old_lines_count: u64,
    pub new_start_line: u64,
    pub new_lines_count: u64,
    pub diff: String,
}

/// Diff index header information
#[derive(Serialize, Clone)]
pub struct DiffIndexHeader {
    pub hash1: String,
    pub hash2: String,
    pub mode: Option<u64>,
}

#[derive(Serialize, Clone)]
pub enum ExtendedDiffHeader {
    OldMode(u64),
    NewMode(u64),
    DeletedFileMode(u64),
    NewFileMode(u64),
    CopyFrom(String),
    CopyTo(String),
    RenameFrom(String),
    RenameTo(String),
    SimilarityIndex(u64),
    DissimilarityIndex(u64),
    Index(DiffIndexHeader),
}

/// Information about single file change in a commit
#[derive(Serialize, Clone)]
pub struct DiffFile {
    pub source: String,
    pub dest: String,
    pub extended_headers: Vec<ExtendedDiffHeader>,
    pub diff_sections: Vec<DiffFileSection>,
    pub is_binary: bool,
}

/// Full commit metadata
#[derive(Serialize)]
pub struct FullCommit {
    pub metadata: CommitMetadata,
    pub diffs: Vec<DiffFile>,
}


/// Objects => commits mapping
#[derive(Serialize)]
pub struct ObjectsToCommitsMappingResponse {
    pub commits: Vec<CommitMetadata>,
    pub mapping: HashMap<String, String>,
}

/// Repo statistics
#[derive(Serialize)]
pub struct RepoStats {
    pub size: u64,
    pub number_commits: u64,
}